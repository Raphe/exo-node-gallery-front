import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonToggleModule, MatButtonModule, MatDialogModule, MatCardModule} from '@angular/material';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { ArtistsService } from './artists.service';
import { MessageService } from './message.service';
import { HttpClientModule } from '@angular/common/http';
import { ArtistDetailComponent, ImgDialog } from './artist-detail/artist-detail.component';
import { SearchbarComponent } from './searchbar/searchbar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ArtistDetailComponent,
    ImgDialog,
    SearchbarComponent
  ],
  entryComponents: [ ArtistDetailComponent, ImgDialog ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatListModule,
    MatGridListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule
  ],
  providers: [
    ArtistsService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
