import { Component, OnInit } from '@angular/core';
import { ArtistsService } from './../artists.service';
import { Artist } from './../artist';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  artists:Artist[] = [];

  constructor(private artistsSvc: ArtistsService) { }

  ngOnInit() {  
    this.getArtist();
  }

  getArtist():void{
    this.artistsSvc.getArtist()
    .subscribe(artists => {
      artists.forEach(artist => {
        this.artists.push(Artist.hydrate(artist));
      })
      //console.log(this.artists);
      
    });
  }

}
