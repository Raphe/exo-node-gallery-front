import { Component, OnInit } from '@angular/core';
import { ArtistsService } from './artists.service';
import { Artist } from './artist';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  artists:Artist[] = [];
  events: string[] = [];
  opened: boolean;

  title = 'Artists Galleries';

  constructor(private artistsSvc: ArtistsService) { }
  
  ngOnInit() {  
    this.getArtist();
  }

  getArtist():void{
    this.artistsSvc.getArtist()
    .subscribe(artists => {
      //console.log(this.artists);
      artists.forEach(artist => {
        this.artists.push(Artist.hydrate(artist));
      })
    });
  }
}
