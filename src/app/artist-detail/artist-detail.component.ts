import { Component, OnInit, Inject } from '@angular/core';
import { ArtistsService } from './../artists.service';
import { Artist } from './../artist';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export interface DialogData {
  src: string;
}

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent implements OnInit {
  artist:Artist;// = new Artist;
  //src: string;
  constructor(private artistsSvc:ArtistsService, private route:ActivatedRoute, private router:Router, public dialog: MatDialog) { }

  ngOnInit() {  
    this.getOneArtist();
  }

  getOneArtist():void{
    const id = this.route.snapshot.paramMap.get('id');
    
    this.artistsSvc.getOneArtist(id)
    .subscribe(artist => {
        this.artist = Artist.hydrate(artist);
      })
  }

  openDialog(src:string): void {
    const dialogRef = this.dialog.open(ImgDialog, {
      data: {src}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

@Component({
  selector: 'img-dialog',
  templateUrl: 'img-dialog.html',
})
export class ImgDialog {

  constructor(
    public dialogRef: MatDialogRef<ImgDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
