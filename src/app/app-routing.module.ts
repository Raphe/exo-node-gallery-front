import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';

const routes: Routes = [
  {path:'', redirectTo:'/dashboard', pathMatch:'full'},
  {path:'dashboard', component: DashboardComponent},
  {path:'artist/:id', component: ArtistDetailComponent}
];

@NgModule({
  imports: [ 
    RouterModule.forRoot(routes, {onSameUrlNavigation:'reload'})
  ],
  declarations: [],
  exports: [ 
    RouterModule
  ]
})
export class AppRoutingModule { }
