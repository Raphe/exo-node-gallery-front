import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Artist } from './artist';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ArtistsService {
  private apiUrl = "http://localhost:3000/";

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  getArtist(): Observable<Artist[]>{
    const artistUrl = this.apiUrl + 'list';
    return this.http.get<Artist[]>(artistUrl)
    .pipe(
      tap(artistes => this.log('fetched artistes')),
      catchError(this.handleError('getArtist', []))
    );
  }

  getOneArtist(id:string): Observable<Artist>{
    const oneArtistUrl = this.apiUrl + 'details/' + id;
    return this.http.get<Artist | any>(oneArtistUrl)
    .pipe(
      tap(artist => this.log('fetched artistes')),
      catchError(this.handleError('getOneArtist', []))
    );
  }

  searchArtist(term: string): Observable<Artist[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Artist[]>(`${this.apiUrl}search/${term}`).pipe(
      tap(_ => this.log(`Artiste trouvé : "${term}"`)),
      catchError(this.handleError<Artist[]>('searchArtiste', []))
    );
  }

  /**
* Gestion requete Http manquées
* pour faire que l'app ne s'arrête pas.
* @param operation - nom de l'operation manquée
* @param result - valeur optionnelle. Retourne un observable.
*/
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    // console.error(error);
    // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

  /** Message de log avec MessageService */
  private log(message: string) {
    this.messageService.add(`ArtistsService: ${message}`);
  }
}
