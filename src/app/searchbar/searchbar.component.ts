import { Component, OnInit } from '@angular/core';
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

 import { Artist } from '../artist';
 import { ArtistsService } from '../artists.service'
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {
  artists$: Observable<Artist[]>;
  private searchTerms = new Subject<string>();
  constructor(private artistsSvc:ArtistsService) { }

    // Envoie le terme dans le flux de l'observable
    search(term: string): void {
      this.searchTerms.next(term);
    }
  
    ngOnInit(): void {
      this.artists$ = this.searchTerms.pipe(
        // On attend 300ms après la frappe pour evaluer le terme
        debounceTime(300),
  
        // On ne fait rien si c'est le meme terme
        distinctUntilChanged(),
  
        // Si le terme à changé alors on switch vers une nouvelle recherche
        switchMap((term: string) => this.artistsSvc.searchArtist(term)),
      );
    }

  

}
