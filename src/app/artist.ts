export class Artist {
  id: string;
  name: string;
  bio:string;
  expo:Array<string> = [];
  pics:Array<string> = [];
  formatName():string{
    var n = this.name.trim().split(" ");
    var lastWord =  n[n.length - 1];
    return lastWord.toLowerCase();
  }

  setPics():void{//:Array<string>{
    
    this.expo[0].photos.forEach(photo => {
      
      this.pics.push("http://localhost:3000/images/"+this.formatName()+"/"+photo.image.title);
    });
    
    //this.expos.photos.forEach(){

    //}
   // "http://localhost:3000/images/"+formatName+"/"
  }

  static hydrate(data:Object):Artist{
    let artist = new Artist;
    if(data.hasOwnProperty('_id')){
      artist.id = data._id;
    }
    if(data.hasOwnProperty('name')){
      artist.name = data.name;
    }
    if(data.hasOwnProperty('bio')){
      artist.bio = data.bio;
    }
    if(data.hasOwnProperty('expo')){
      artist.expo = data.expo;
    }

    artist.setPics();
    return artist;
  }
}
